shareUrl = window.location

ig.fit = ->
  return unless $?
  $body = $ 'body'
  # $ 'iframe[allowfullscreen]' .attr 'src' 'https://www.youtube.com/embed/zfZg2r5Dz-Q'

  $video = $ "<video src='https://samizdat.blob.core.windows.net/zvuky/maraton-cover-video.mp4' autoplay loop>"
    ..bind \play -> $video.addClass "playing"
  $hero = $ "<div class='hero'></div>"
    ..append $video
    ..append "<div class='overlay'></div>"
    # ..append "<span class='copy'>foto: Jonáš Mlejnek, Ondřej Brom ml.</span>"
    ..append "<a href='#' class='scroll-btn'></a>"
    ..find 'a.scroll-btn' .bind 'click touchstart' (evt) ->
      evt.preventDefault!
      offset = $filling.offset!top + $filling.height! - 50
      d3.transition!
        .duration 800
        .tween "scroll" scrollTween offset
  $body.prepend $hero
  $filling = $ "<div class='ig filling'></div>"
    ..css \height $hero.height! + 50
  $ '#article h1' .html "<span>Jak bolí maraton?</span>"
  $ "p.perex"
    .html "Senzory změří, co se děje s tělem svátečního běžce"
    .after $filling
  $filling
    .after "<p>Stačí člověku, který míval ke sportu odtažitý vztah, a ještě před rokem vážil 125 kilo, na zvládnutí maratonu osmiměsíční trénink? Na tuhle otázku odpoví Alex Trejtnar (42), který se rozhodl uběhnout 42&nbsp;kilometrů s kamerou na hlavě. Pod dohledem senzorů monitorujících jeho životní funkce vystaví svůj organismu extrémní zkoušce. Přímý přenos toho, jak to dopadne, můžete sledovat v neděli 8. května 2016 od 9 hodin ráno na této webové stránce.</p>"

  $shares = $ "<div class='shares'>
    <a class='share cro' title='Zpět nahoru' href='#'><img src='https://samizdat.cz/tools/cro-logo/cro-logo-light.svg'></a>
    <a class='share fb' title='Sdílet na Facebooku' target='_blank' href='https://www.facebook.com/sharer/sharer.php?u=#shareUrl'><img src='https://samizdat.cz/tools/icons/facebook-bg-white.svg'></a>
    <a class='share tw' title='Sdílet na Twitteru' target='_blank' href='https://twitter.com/home?status=#shareUrl'><img src='https://samizdat.cz/tools/icons/twitter-bg-white.svg'></a>
  </div>"
  $body.prepend $shares
  sharesTop = $shares.offset!top
  sharesFixed = no

  $ window .bind \resize ->
    $shares.removeClass \fixed if sharesFixed
    sharesTop := $shares.offset!top
    $shares.addClass \fixed if sharesFixed
    $filling.css \height $hero.height! + 50


  $ window .bind \scroll ->
    top = (document.body.scrollTop || document.documentElement.scrollTop)
    if top > sharesTop and not sharesFixed
      sharesFixed := yes
      $shares.addClass \fixed
    else if top < sharesTop and sharesFixed
      sharesFixed := no
      $shares.removeClass \fixed
  $shares.find "a[target='_blank']" .bind \click ->
    window.open do
      @getAttribute \href
      ''
      "width=550,height=265"
  $shares.find "a.cro" .bind \click (evt) ->
    evt.preventDefault!
    d3.transition!
      .duration 800
      .tween "scroll" scrollTween 0
  <~ $
  $ '#aside' .remove!

scrollTween = (offset) ->
  ->
    interpolate = d3.interpolateNumber do
      window.pageYOffset || document.documentElement.scrollTop
      offset
    (progress) -> window.scrollTo 0, interpolate progress
